# created by Martin Osusky

# use official image
FROM python:3

# set the project directory
WORKDIR /usr/src/app

# install dependencies
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# copy the whole project
COPY . .

# set default python project diretory; pytest reason
ENV PYTHONPATH /usr/src/app

# server runtime
CMD flask db upgrade && python ./run.py