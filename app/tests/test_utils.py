from app import utils
from app.schema import history, date, integer, search


def test_validate():
    data = {'page': '1', 'per_page': '1', 'search': 'text', 'date_from': '2020-03-19T07:12:12Z',
            'date_to': '2020-03-20T12:12:12Z'}
    schema = {'date_from': date, 'date_to': date, 'search': search, 'page': integer,
           'per_page': integer}

    assert utils.validate(data, schema)

"""
todo finish other and more relevant tests
"""