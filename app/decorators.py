from flask import request

from app.utils import log


def log_json(func):
    def wrapper(*args, **kwargs):
        log(request.json)
        return func(*args, **kwargs)

    return wrapper


def log_get(func):
    def wrapper(*args, **kwargs):
        log(request.get_data())
        return func(*args, **kwargs)

    return wrapper