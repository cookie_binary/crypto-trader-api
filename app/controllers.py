from dateutil.relativedelta import relativedelta
from flask import Blueprint, request
from sqlalchemy import exc, or_
from app.decorators import log_json, log_get
from app.exceptions import AppErrorHandler
from app.models import Exchange, CryptoCurrency, Trade, TradeRequest
from app.utils import list_of_models_to_json, validate
from app import schema, db, app
from app.repository import Repository
import datetime

api_crypto = Blueprint("api", __name__, url_prefix="/api/v1/crypto")


@log_json
@app.route('/', methods=['GET'])
def welcome():
    return {"msg": "Welcome to Crypto Trader API"}


@log_json
@api_crypto.route('/exchanges', methods=['POST'])
def add_crypto_exchange():
    """
    body should contain name (its presence should be validated) and currency in which total amount for exchange will be displayed
    response: new created object
    """
    validate(request.json, schema.exchange)
    exchange = Exchange(name=request.json['name'], currency=request.json['currency'])
    db.session.add(exchange)
    db.session.commit()

    return exchange.as_dict


@log_json
@api_crypto.route('/exchanges/<int:exchange_id>', methods=['GET'])
def exchange_status(exchange_id):
    """Response information about required exchange"""
    exchange = Repository.get_exchange(exchange_id)

    return exchange.as_dict


@log_json
@api_crypto.route('/exchanges/<int:exchange_id>', methods=['POST'])
def deposit_exchange(exchange_id):
    """body should contain only amount (Float), e.g. 1000 (currency is taken from corresponding exchange)"""
    exchange = Repository.get_exchange(exchange_id)
    validate(request.json, schema.deposit)

    if request.json['amount'] > exchange.total_amount:
        raise AppErrorHandler('Not enough balance on trade exchange')

    exchange.deposit(request.json['amount'])

    # response: not only "success" status but whole exchange model
    return exchange.as_dict


@log_json
@api_crypto.route('/exchanges/<int:exchange_id>/currencies', methods=['PUT'])
def update_cryptocurrencies_within_exchange(exchange_id):
    """
    crypto-currency object should contains minimal: 3 letter shortcut, actual rate for the given exchange currency
    bulk request -> body should contain created/deleted or updated cryptocurrencies (when preferred new crypto-currency)
    response: all cryptocurrencies belongs to exchange
    """
    exchange = Repository.get_exchange(exchange_id)

    try:
        validate(request.json, schema.currencies_bulk)

        # create section
        create = request.json['create']
        for currency_rate in create:
            # iterate inputs
            validate(currency_rate, schema.currency_update)

            # create new crypto currency
            # check if exists
            if CryptoCurrency.query.filter_by(exchange=exchange, code=currency_rate['code']).count():
                raise AppErrorHandler("Crypto currency already exists")

            crypto_currency = CryptoCurrency(code=currency_rate['code'],
                                             rate=currency_rate['rate'],
                                             exchange=exchange)
            db.session.add(crypto_currency)

        # update section
        update = request.json['update']
        for currency_rate in update:
            # iterate inputs
            validate(currency_rate, schema.currency_update)

            # create new crypto currency
            crypto_currency = CryptoCurrency.query.filter_by(exchange=exchange, code=currency_rate['code']).first()
            if not crypto_currency:
                raise AppErrorHandler("You're trying to update a non existing currency")

            crypto_currency.rate = currency_rate['rate']
            db.session.add(crypto_currency)

        # delete section
        delete = request.json['delete']
        for currency_rate in delete:
            # iterate inputs
            validate(currency_rate, {'code': schema.crypto_currency})

            # create new crypto currency
            crypto_currency = CryptoCurrency.query.filter_by(exchange=exchange, code=currency_rate['code']).first()
            if not crypto_currency:
                raise AppErrorHandler("You're trying to delete a non existing currency")
            db.session.delete(crypto_currency)

        db.session.commit()
    except exc.SQLAlchemyError as err:
        raise AppErrorHandler("Database integrity ERROR")

    return list_of_models_to_json(exchange.crypto_currencies)


@log_json
@api_crypto.route('/exchanges/<int:exchange_id>/trades', methods=['POST'])
def create_trade(exchange_id):
    """
     body should contains amount, currency_in, currency_out after trade was made, converted amount
     should be automatically added to/subtracted from total
     exchange amount and actual exchange currency amount (depends on operation)
    """
    exchange = Repository.get_exchange(exchange_id)

    # validate
    validate(request.json, schema.trade)

    # request record
    trade_request = TradeRequest(amount=request.json['amount'],
                                 currency_in=request.json['currency_in'],
                                 currency_out=request.json['currency_out'],
                                 exchange=exchange)

    db.session.add(trade_request)
    # commit here; for record
    db.session.commit()

    # deciding whether it is a purchase or a sale
    if exchange.currency == request.json['currency_in']:
        crypto_currency_code = request.json['currency_out']
        trade_type = 'purchase'
    elif exchange.currency == request.json['currency_out']:
        crypto_currency_code = request.json['currency_in']
        trade_type = 'sale'
    else:
        raise AppErrorHandler(f'In this exchange it is necessary to trade with currency {exchange.currency}.')

    # find the appropriate crypto currency
    crypto_currency = CryptoCurrency.query.filter_by(exchange=exchange, code=crypto_currency_code).first()
    if not crypto_currency:
        raise AppErrorHandler("Crypto currency not found")

    # exchange rate conversion
    # fee is not implemented
    crypto_amount = request.json['amount']
    exchange_amount = crypto_amount * float(crypto_currency.rate)

    # record trade
    trade = Trade(type=trade_type,
                  crypto_amount=crypto_amount,
                  exchange_amount=exchange_amount,
                  crypto_currency=crypto_currency,
                  exchange=exchange,
                  trade_request=trade_request)

    # exchange - total balance application
    exchange.total_amount = float(exchange.total_amount) + (
        exchange_amount if trade_type == 'purchase' else - exchange_amount)

    if exchange.total_amount < 0:
        raise AppErrorHandler("Insufficient funds in this exchange")

    trade_request.status = 'successful'

    # finally write to the database
    db.session.add(trade)
    db.session.add(trade_request)
    db.session.add(exchange)
    db.session.commit()

    return trade.as_dict


@log_get
@api_crypto.route('/history', methods=['GET'])
def history_of_trades():
    # validate
    validate(request.args.to_dict(), schema.history)

    # load filters
    date_from = request.args.get('date_from')
    date_to = request.args.get('date_to')
    exchange_id = request.args.get('exchange_id')
    search = request.args.get('search')
    page = request.args.get('page')
    per_page = request.args.get('per_page')

    # query building section
    query_builder = TradeRequest.query.filter_by(status='successful')

    if date_from:
        query_builder = query_builder.filter(Trade.created_at >= date_from)

    if date_to:
        date_to = datetime.datetime.strptime(date_to, "%Y-%m-%dT%H:%M:%S%z") + relativedelta(days=1)
        query_builder = query_builder.filter(Trade.created_at < date_to)

    if exchange_id:
        query_builder = query_builder.filter_by(exchange_id=exchange_id)

    if search:
        query_builder = query_builder.filter(
            or_(TradeRequest.currency_in.like(f'%{search}%'), TradeRequest.currency_out.like(f'%{search}%')))

    # query execution
    if page and per_page:
        history = query_builder.paginate(int(page), int(per_page), error_out=False).items
    else:
        history = query_builder.all()

    return list_of_models_to_json(history)
