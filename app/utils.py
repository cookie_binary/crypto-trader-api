from app import db
from cerberus import Validator
from flask import jsonify, abort, make_response
from app.exceptions import AppErrorHandler
import sys


def simplify_list(objects, property_name):
    return [getattr(o, property_name) for o in objects]


def list_of_models_to_json(models):
    return jsonify(simplify_list(models, 'as_dict'))


def log(*args, **kwargs):
    for arg in args:
        print(arg, file=sys.stderr)


def return_error(errors, http_code=400):
    # todo use exceptions throwing
    abort(make_response(jsonify(message=errors), http_code))


def validate(data, schema):
    # validate
    validator = Validator()
    if not validator.validate(data, schema):
        raise AppErrorHandler(validator.errors)
    return True


def db_add_commit(*args, **kwargs):
    for model in args:
        db.session.add(model)
        db.session.commit()

