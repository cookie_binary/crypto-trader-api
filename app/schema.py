# deprecated
from app.models import Exchange
from datetime import datetime


def check_currency(field, value, error):
    if len(str(value)) < 3:
        error(field, 'error message')


def unique_exchange_name(field, value, error):
    if Exchange.query.filter_by(name=value).first():
        error(field, 'Exchange with name "' + value + '" already exists')


def exchange_exists(field, value, error):
    if not Exchange.query.get(value):
        error(field, 'Exchange ID:"' + value + '" does not exist')


def all_optional_forced(a_dict):
    non_required = {'required': False}
    return {key: {**value, **non_required} for key, value in a_dict.items()}


def check_integers(field, value, error):
    if str(int(value)) != value:
        error(field, 'Must be integer')


# to_date = lambda s: datetime.strptime(s, '%Y-%m-%d')

# elements
name = {'type': 'string', 'required': True, 'minlength': 3, 'check_with': unique_exchange_name}
currency = {'type': 'string', 'required': True, 'minlength': 3}
crypto_currency = {'type': 'string', 'required': True, 'minlength': 3, 'maxlength': 3}
deposit_amount = {'type': 'number', 'min': 0.01}
# deposit_amount = {}
rate = {'type': 'number', 'min': 0.000000001}
amount = {'type': 'number', 'min': 0.000000001}
listed = {'type': 'list', 'required': True}
date = {'type': 'datetime', 'coerce': lambda s: datetime.strptime(s, "%Y-%m-%dT%H:%M:%S%z")}
exchange_id = {'check_with': exchange_exists}
search = {'type': 'string'}
integer = {'check_with': check_integers}

# schemas
exchange = {'name': name, 'currency': currency}
deposit = {'amount': deposit_amount}
currencies_bulk = {'create': listed, 'update': listed, 'delete': listed}
currency_update = {'code': crypto_currency, 'rate': rate}
trade = {'amount': amount, 'currency_in': currency, 'currency_out': currency}
history = {'date_from': date, 'date_to': date, 'exchange_id': exchange_id, 'search': search, 'page': integer,
           'per_page': integer}
