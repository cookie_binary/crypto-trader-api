from app.models import Exchange


# todo move this somewhere else & refactor
class Repository:
    @staticmethod
    def get_exchange(exchange_id):
        return Exchange.query.get(exchange_id)
