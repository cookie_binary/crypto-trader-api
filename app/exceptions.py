from flask import Blueprint, jsonify

errors = Blueprint('errors', __name__)


class AppErrorHandler(Exception):
    """Base App error handler."""


@errors.app_errorhandler(AppErrorHandler)
def handle_error(error):
    # message = [str(x) for x in error.args]
    message = error.args
    try:
        status_code = error.status_code
    except AttributeError:
        status_code = 400

    success = False
    response = {
        'success': success,
        'error': {
            'type': error.__class__.__name__,
            'message': message
        }
    }

    return jsonify(response), status_code
