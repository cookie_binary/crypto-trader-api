import decimal

from app import db
from sqlalchemy import Column
from sqlalchemy.orm import relationship
import datetime


# Abstract model template
class BaseModel(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)

    @property
    def as_dict(self):
        # todo not everything must be string
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}


# Exchange model
class Exchange(BaseModel):
    __tablename__ = 'exchange'
    name = db.Column(db.String(200), nullable=False)
    # todo precisions to application config
    total_amount = db.Column(db.DECIMAL(precision=20, scale=10), nullable=False, default=0)
    currency = db.Column(db.String(50), nullable=False)
    created_at = Column(db.DateTime, default=datetime.datetime.utcnow)

    # relations
    crypto_currencies = db.relationship('CryptoCurrency', back_populates='exchange')
    trades = db.relationship('Trade', back_populates='exchange')
    trade_requests = db.relationship('TradeRequest', back_populates='exchange')

    # operations
    def deposit(self, amount):
        self.total_amount = self.total_amount - decimal.Decimal(amount)
        db.session.add(self)
        db.session.commit()


# ## Crypto Currency Model
class CryptoCurrency(BaseModel):
    __tablename__ = 'crypto_currency'
    __table_args__ = (
        db.UniqueConstraint('code', 'exchange_id', name='unique_currency_commit'),
    )
    code = db.Column(db.String(3), nullable=False)
    rate = db.Column(db.DECIMAL(precision=20, scale=10), nullable=False)
    exchange_id = Column(db.Integer, db.ForeignKey('exchange.id'))
    exchange = relationship("Exchange", back_populates="crypto_currencies")
    created_at = Column(db.DateTime, default=datetime.datetime.utcnow)
    trades = db.relationship('Trade', back_populates='crypto_currency')


# ## Trade Request Model
class TradeRequest(BaseModel):
    __tablename__ = 'trade_request'
    amount = db.Column(db.DECIMAL(precision=20, scale=10), nullable=False)
    currency_in = db.Column(db.String(3), nullable=False)
    currency_out = db.Column(db.String(3), nullable=False)
    exchange_id = Column(db.Integer, db.ForeignKey('exchange.id'))
    exchange = relationship("Exchange", back_populates="trade_requests")
    created_at = Column(db.DateTime, default=datetime.datetime.utcnow)
    trades = db.relationship('Trade', back_populates='trade_request', uselist=False)
    status = db.Column(db.String(20), default='pending')


# ## Trade Model
class Trade(BaseModel):
    __tablename__ = 'trade'
    type = db.Column(db.String(20), nullable=False)  # possible values ['purchase', 'sale']
    crypto_amount = db.Column(db.DECIMAL(precision=20, scale=10), nullable=False)  # in crypto currency
    exchange_amount = db.Column(db.DECIMAL(precision=20, scale=10), nullable=False)  # in appropriate exchange currency

    # relations
    crypto_currency_id = db.Column(db.Integer, db.ForeignKey('crypto_currency.id'))
    crypto_currency = relationship("CryptoCurrency", back_populates="trades")

    exchange_id = db.Column(db.Integer, db.ForeignKey('exchange.id'))
    exchange = relationship("Exchange", back_populates="trades")

    trade_request_id = db.Column(db.Integer, db.ForeignKey('trade_request.id'))
    trade_request = relationship("TradeRequest", back_populates="trades")

    created_at = Column(db.DateTime, default=datetime.datetime.utcnow)