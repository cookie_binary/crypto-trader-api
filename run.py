from app import app

if __name__ == "__main__":
    """
    host limitation and port is defined in docker-compose.yml or .env
    Leave restrictions for localhost and port here as is
    """
    app.run(debug=True, host='0.0.0.0', port=5000)
