"""empty message

Revision ID: 477976e722a3
Revises: 
Create Date: 2020-03-19 07:36:41.720055

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '477976e722a3'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('exchange',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=200), nullable=False),
                    sa.Column('total_amount', sa.DECIMAL(precision=20, scale=10), nullable=False),
                    sa.Column('currency', sa.String(length=50), nullable=False),
                    sa.Column('created_at', sa.DateTime(), nullable=True),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('crypto_currency',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('code', sa.String(length=3), nullable=False),
                    sa.Column('rate', sa.DECIMAL(precision=20, scale=10), nullable=False),
                    sa.Column('exchange_id', sa.Integer(), nullable=True),
                    sa.Column('created_at', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(['exchange_id'], ['exchange.id'], ),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('code', 'exchange_id', name='unique_currency_commit')
                    )
    op.create_table('trade_request',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('amount', sa.DECIMAL(precision=20, scale=10), nullable=False),
                    sa.Column('currency_in', sa.String(length=3), nullable=False),
                    sa.Column('currency_out', sa.String(length=3), nullable=False),
                    sa.Column('exchange_id', sa.Integer(), nullable=True),
                    sa.Column('created_at', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(['exchange_id'], ['exchange.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('trade',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('type', sa.String(length=20), nullable=False),
                    sa.Column('crypto_amount', sa.DECIMAL(precision=20, scale=10), nullable=False),
                    sa.Column('exchange_amount', sa.DECIMAL(precision=20, scale=10), nullable=False),
                    sa.Column('crypto_currency_id', sa.Integer(), nullable=True),
                    sa.Column('exchange_id', sa.Integer(), nullable=True),
                    sa.Column('trade_request_id', sa.Integer(), nullable=True),
                    sa.Column('created_at', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(['crypto_currency_id'], ['crypto_currency.id'], ),
                    sa.ForeignKeyConstraint(['exchange_id'], ['exchange.id'], ),
                    sa.ForeignKeyConstraint(['trade_request_id'], ['trade_request.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('trade')
    op.drop_table('trade_request')
    op.drop_table('crypto_currency')
    op.drop_table('exchange')
    # ### end Alembic commands ###
